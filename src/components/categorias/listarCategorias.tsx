import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button } from "reactstrap";
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { editarCategoria, eliminarCategoria, obtenerCategorias } from '../../services/categorias.services';
import { Categoria } from '../../interfaces/categoria';

export function ListarCategorias() {
    const [categorias, setCategorias] = useState([]);

    useEffect(() => {
        obtenerCategorias().then((resultado: any)=> {
            setCategorias(resultado);
        })
    }, []);

 

    const editarDato = (categoria: Categoria) => {
        editarCategoria(categoria).then((resultado: any) => {
            if (resultado) {
                Swal.fire({
                    title: `Categoria editada correctamente`,
                    text: ``,
                    icon: `success`
                })  
            }else{
                Swal.fire({
                    title: `Error al editar categoria`,
                    text: ``,
                    icon: `error`
                })
            }
            
        }).catch(() => {
            Swal.fire({
                title: `Error al editar categoria`,
                text: ``,
                icon: `success`
            })
        })
    };

    const cancelarDato = (categoria: Categoria) => {
        Swal.fire({
            title: `¿ Eliminar categoria ${categoria.id} ?`,
            html: "",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Confirmar!',
            cancelButtonText: 'Cerrar',
            reverseButtons: true,
            showLoaderOnConfirm: true,
            allowEscapeKey: false,
            allowOutsideClick: () => !Swal.isLoading(),
          }).then((result) => {
            if (result.isConfirmed) {
                //
                eliminarCategoria(categoria).then((resultado: any) => {
                    if (resultado) {
                        Swal.fire({
                            title: `Categoria editada correctamente`,
                            text: ``,
                            icon: `success`
                        })  
                    }else{
                        Swal.fire({
                            title: `Error al editar categoria`,
                            text: ``,
                            icon: `error`
                        })
                    }
                    
                }).catch(() => {
                    Swal.fire({
                        title: `Error al editar categoria`,
                        text: ``,
                        icon: `success`
                    })
                })
            }
            //
          });

    };

    return (
        <>
            <Table>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>OPCIONES</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        categorias.map((dato: any) => (
                            <tr key={dato.id}>
                                <td>{dato.id}</td>
                                <td>{dato.nombre}</td>
                                <td>
                                    <Button color='info' style={{ marginRight: '10px' }} onClick={() => editarDato(dato)}>Editar</Button>
                                    <Button color='danger' onClick={() => cancelarDato(dato)}>Cancelar</Button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </>
    );
}
