export interface Categoria{
    id?: number,
    nombre?: string,
    rfc?: string,
    codifo?: string
}