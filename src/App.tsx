
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import {  ListarCategorias } from "./components/categorias/listarCategorias";



function App() {
  return (
  <BrowserRouter>
    <Routes>
      <Route path='/' element={<ListarCategorias/>} ></Route>
    </Routes>
  </BrowserRouter>
  )
}

export default App
