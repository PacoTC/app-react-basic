import axios from "axios";
import { Categoria } from "../interfaces/categoria";

export const obtenerCategorias = async () => {
    try {
        const { data, status } = await axios.get("https://660e34ce6ddfa2943b3610fd.mockapi.io/proveedores");
        if (status !== 200) {
            console.log("error",data);
            return []
        }
        return data;
    } catch (error) {
        console.log("error",error);
        return []
    }
}


export const obtenerCategoriaId = async (categoria:Categoria) => {
    try {
        const { data, status } = await axios.get(`https://660e34ce6ddfa2943b3610fd.mockapi.io/proveedores/${categoria.id}`);
        if (status !== 200) {
            console.log("error",data);
            return undefined
        }
        return data;
    } catch (error) {
        console.log("error",error);
        return undefined
    }

}


export const eliminarCategoria = async (categoria:Categoria) => {
    try {
        const { data, status } = await axios.delete(`https://660e34ce6ddfa2943b3610fd.mockapi.io/proveedores/${categoria.id}`);
        if (status !== 200) {
            console.log("error",data);
            return undefined
        }
        return data;
    } catch (error) {
        console.log("error",error);
        return undefined
    }

}

export const editarCategoria = async (categoria:Categoria) => {
    try {
        const { data, status } = await axios.put(
            `https://660e34ce6ddfa2943b3610fd.mockapi.io/proveedores/${categoria.id}`
            ,categoria);
        if (status !== 200) {
            console.log("error",data);
            return []
        }
        return data;
    } catch (error) {
        console.log("error",error);
        return []
    }

}



export const crearCategorias = async (categoria:Categoria) => {
    try {
        const { data, status } = await axios.post(
            `https://660e34ce6ddfa2943b3610fd.mockapi.io/proveedores`
            ,categoria);
        if (status !== 200) {
            console.log("error",data);
            return []
        }
        return data;
    } catch (error) {
        console.log("error",error);
        return []
    }

}